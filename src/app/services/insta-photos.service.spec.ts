import { TestBed, inject } from '@angular/core/testing';

import { InstaPhotosService } from './insta-photos.service';

describe('InstaPhotosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstaPhotosService]
    });
  });

  it('should be created', inject([InstaPhotosService], (service: InstaPhotosService) => {
    expect(service).toBeTruthy();
  }));
});
