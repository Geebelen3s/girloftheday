import { TestBed, inject } from '@angular/core/testing';

import { InstaJSONService } from './insta-json.service';

describe('InstaJSONService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstaJSONService]
    });
  });

  it('should be created', inject([InstaJSONService], (service: InstaJSONService) => {
    expect(service).toBeTruthy();
  }));
});
