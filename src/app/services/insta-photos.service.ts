import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { InstaPhoto } from '../models/insta-photo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InstaPhotosService {
  photosCollection: AngularFirestoreCollection<InstaPhoto>;
  photos: Observable<InstaPhoto[]>;

  constructor(
      public angularFireStore: AngularFirestore,
  ) {
    this.photosCollection = this.angularFireStore.collection<InstaPhoto>('instagram_photos');
    // this.photos = this.photosCollection.valueChanges();
    this.photos = this.photosCollection.snapshotChanges().pipe(
        map((actions) => {
          return actions.map((action) => {
              const data = action.payload.doc.data() as InstaPhoto;
              const id = action.payload.doc.id;
              return { id, ...data };
          });
        })
    );
  }
  getPhotos() {
    return this.photos;
  }

    updateLikes(id, value) {
        this.photosCollection.doc(id).update({ photoLikes: value });
    }
    addDatePlaced(id, value) {
        this.photosCollection.doc(id).update({ datePlaced: value });
    }
}
