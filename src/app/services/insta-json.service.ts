import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InstaContent } from 'models/InstaContent';

@Injectable({
  providedIn: 'root'
})
export class InstaJSONService {
    apiRoot: string = 'https://api.instagram.com/';
    private apiURL: string;

    constructor(private http: HttpClient) {
    }
    search(term: string): Observable<any> {
        this.apiURL = `${this.apiRoot}oembed?url=http://instagr.am/p/${term}/&omitscript=true&callback=JSON_CALLBACK`;
        return this.http.jsonp(this.apiURL, 'callback');
    }
}