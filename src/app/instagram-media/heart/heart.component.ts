import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-heart',
  templateUrl: './heart.component.html',
  styleUrls: ['./heart.component.css']
})
export class HeartComponent implements OnInit {
  public heartActive = false;
  public heartCount = 0;
  constructor() { }

  ngOnInit() {
  }
  public updateCount() {
      if (this.heartActive) {
          this.heartCount = this.heartCount - 1;
          this.heartActive = false;
      } else {
          this.heartCount = this.heartCount + 1;
          this.heartActive = true;
      }
  }
}
