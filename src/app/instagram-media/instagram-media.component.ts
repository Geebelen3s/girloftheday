import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { InstaJSONService } from '../services/insta-json.service';
import { InstaContent } from '../models/insta-content';

import { InstaPhotosService } from '../services/insta-photos.service';
import { Meta } from '@angular/platform-browser';
import { DateService } from '../services/date.service';


@Component({
  selector: 'app-instagram-media',
  templateUrl: './instagram-media.component.html',
  styleUrls: ['./instagram-media.component.css']
})
export class InstagramMediaComponent implements OnInit {
  @Input() public InstaID: string;
  @Output() InstaIDChange = new EventEmitter<string>();
  allPhotos = [];
  currentDate: string;
  currentPhoto = <any>{};
  src: string;
  photoWithCurrentDate: any[];
  photosWithEmptyDates: any[];
public heartActive = false;
public heartCount = 0;
  defaultURL = 'https://media.giphy.com/media/xTkcEQACH24SMPxIQg/giphy.gif';

  InstaJSON: any = {};
    constructor(
        private instaJsonService: InstaJSONService,
        private instaPhotoService: InstaPhotosService,
        private meta: Meta,
        private date: DateService,
    ) {
        this.currentDate = date.getCurrentDate();
    }

  ngOnInit() {
        this.getAllPhotos();
        console.log('heartCount', this.heartCount);
        if (localStorage.getItem('voted') === this.currentDate) {
            this.heartActive = true;
        } else {
            this.heartActive = false;
            localStorage.removeItem('voted');
        }
  }
    public updateCount() {
        if (this.heartActive) {
            this.heartCount = this.heartCount - 1;
            this.heartActive = false;
            this.instaPhotoService.updateLikes(this.currentPhoto.id, this.heartCount);
            localStorage.removeItem('voted');
        } else {
            this.heartCount = this.heartCount + 1;
            this.heartActive = true;
            this.instaPhotoService.updateLikes(this.currentPhoto.id, this.heartCount);
            localStorage.setItem('voted', this.currentDate);
        }
    }
    updateMetaTags() {
        this.meta.addTag({ property: 'og:title', content: 'Girl Of The Day: ' + this.InstaJSON.author_name  });
        this.meta.addTag({ property: 'og:type', content: 'website' });
        this.meta.addTag({ property: 'og:url', content: 'http://dev.girloftheday.be' });
        this.meta.addTag( { property: 'og:image', content: `https://instagram.com/p/${this.currentPhoto.photoId}/media/?size=t` });
    }
    updateUrl() {
        this.src = this.defaultURL;
    }

    getAllPhotos() {
        this.instaPhotoService.getPhotos().subscribe((result) => {
                this.allPhotos = result;
                // console.log(this.allPhotos);
                this.getCurrentPhoto();
            }
        );
    }

    getCurrentPhoto() {
        // console.log('currentdate', this.currentDate);
        // this.currentPhoto = this.allPhotos[0];
        // this.updateMetaTags();
        const self = this;
        this.photoWithCurrentDate =  this.allPhotos.filter(function(photo) {
            return photo.datePlaced === self.currentDate;
        });

        // console.log('photowithcurrentdate', this.photoWithCurrentDate);

        if (this.photoWithCurrentDate.length === 0 ) {
            // console.log('date does not exist');
            // There is no photo with the current date, so a random photo needs to be selected from all the photos with an empty date
            // console.log('allphotos', this.allPhotos);
            this.photosWithEmptyDates = this.allPhotos.filter(function(photo) {
                return photo.datePlaced === '';
            });
            // console.log('empty dates', this.photosWithEmptyDates);

            // console.log('photoswithemptydate', this.photosWithEmptyDates);
            this.currentPhoto = this.photosWithEmptyDates[Math.floor(Math.random() * this.photosWithEmptyDates.length)];

            // console.log('random currentphoto', this.currentPhoto);
            // console.log('currentphoto', this.currentPhoto);
            // console.log('id', this.currentPhoto.id);
            // add currentDate to the currentPhoto in the database
            this.instaPhotoService.addDatePlaced(this.currentPhoto.id, this.currentDate);

        } else {
            // console.log('date does exist');

            this.currentPhoto = this.photoWithCurrentDate[0];
            // console.log('currentPhoto', this.currentPhoto);
        }
        this.getInstagramContent();
        // console.log('currentDate', this.currentDate);
        // console.log('object.keys', Object.keys(this.photoWithCurrentDate));
        this.heartCount = this.currentPhoto.photoLikes;
    }

    getInstagramContent() {
        this.InstaIDChange.emit();
        this.src = `https://instagram.com/p/${this.currentPhoto.photoId}/media/?size=l`;
        this.instaJsonService.search(this.currentPhoto.photoId).subscribe(
            (result) => {
                this.InstaJSON =  result;
                console.log(result);
                // this.src = this.InstaJSON.thumbnail_url;
                //  .map( (res)  => {
                //                 // return res.json().results.map( (item) => {
                //                 //     return new InstaContent(
                //                 //         item.author_name,
                //                 //         item.author_url,
                //                 //         item.thumbnail_url,
                //                 //     );
                //                 // });
                // });
            }
        );
    }


}
